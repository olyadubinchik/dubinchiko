package prog.kurs;

import umontreal.ssj.probdist.StudentDist;

import javax.swing.*;
import java.text.DecimalFormat;

public class RegressionUI {

    private Double[] x;
    private Double[] y;

    private Double a;
    private Double b;
    
    private JTextArea console;

    public RegressionUI(Double[] x, Double[] y, JTextArea console) {
        this.x = x;
        this.y = y;
        this.console = console;
        calcRegression();
        regressionEq();
    }

    public String regressionEq() {
        DecimalFormat df = new DecimalFormat("#.####");
        return String.format("y = %sx + %s", df.format(b), df.format(a));
    }

    public Double calculateNew(Double x) {
        return b*x + a;
    }

    private void calcRegression() {
        Double sumX = sumX();
       // this.console.append("Сумма элементов x: " + sumX);
       // this.console.append("\n");

        Double sumY = sumY();
       // this.console.append("Сумма элементов y: " + sumY);
       // this.console.append("\n");

        Double sumSquaresX = sumSquaresX();
       // this.console.append(String.valueOf(sumSquaresX));
      //  this.console.append("\n");

        Double sumXY = sumXY();
       // this.console.append(String.valueOf(sumXY));
       // this.console.append("\n");


        b = b(sumX, sumY, sumXY, sumSquaresX);
        DecimalFormat df = new DecimalFormat("#.####");
        this.console.append("b: " +df.format(b));
        this.console.append("\n");

        a = a(b, sumX, sumY);
        this.console.append("a: " +df.format(a));
        this.console.append("\n");

        this.console.append(regressionEq());
        this.console.append("\n");

        Double srX = srX();
       // this.console.append(String.valueOf(srX));
       // this.console.append("\n");

        Double  sSquaresX =  sSquaresX();
       // this.console.append(String.valueOf(sSquaresX));
       // this.console.append("\n");

        Double  s2 = s2(a,b);
       // this.console.append(String.valueOf(s2));
       // this.console.append("\n");

        Double  sb = sb(s2,sSquaresX);
      //  this.console.append(String.valueOf(sb));
       // this.console.append("\n");

        Double  sa = sa( s2, srX, sSquaresX);
       // this.console.append(String.valueOf(sa));
       // this.console.append("\n");

        double criticalValue = StudentDist.inverseF(x.length - 2, 0.975);
       // this.console.append(String.valueOf(criticalValue));
       // this.console.append("\n");

        boolean isBSufficient = Math.abs(b) > criticalValue * sb;
        this.console.append(isBSufficient ? "Значение коэффициента b значимо" : "Значение коэффициента b не значимо");
        this.console.append("\n");

        boolean isASufficient = Math.abs(b) > criticalValue * sa;
        this.console.append(isASufficient ? "Значение коэффициента a значимо" : "Значение коэффициента a не значимо");
        this.console.append("\n");

        double leftb = leftb(criticalValue, sb);
        double rightb = rightb(criticalValue, sb);
        this.console.append("доверительный интервал для b: [" + df.format(leftb) + ";" + df.format(rightb)+ "]" );
        this.console.append("\n");

        double lefta = lefta(criticalValue, sa);
        double righta = righta(criticalValue, sa);
        this.console.append("доверительный интервал для a: [" + df.format(lefta) + ";" + df.format(righta)+ "]" );
        this.console.append("\n");
    }

    private double sumX() {
        double sumX = 0;
        for (double element : x) {
            sumX += element;
        }
        return sumX;
    }

    private double sumY() {
        double sumY = 0;
        for (double element : y) {
            sumY += element;
        }
        return sumY;
    }

    private double sumSquaresX() {
        double sumSquaresX = 0;
        for (double element : x) {
            sumSquaresX += element * element;
        }
        return sumSquaresX;
    }

    private double sumXY() {
        double sumXY = 0;
        for (int i = 0; i < x.length; i++) {
            sumXY += x[i] * y[i];
        }
        return sumXY;
    }

    private double b(double sumX, double sumY, double sumXY, double sumSquaresX) {
        double length = x.length;
        return (length * sumXY - sumX * sumY) / (length * sumSquaresX - sumX * sumX);
    }

    private double a(double b, double sumX, double sumY) {
        double length = x.length;
        return (sumY - b * sumX) / length;
    }

    private double srX() {
        return sumX()/ x.length;
    }

    private double sSquaresX() {
        double sSquaresX = 0;

        for (int i = 0; i < x.length; i++) {
            sSquaresX += (x[i] - srX())*(x[i] - srX()) ;
        }
        return  sSquaresX / (x.length - 1);
    }

    private double s2 (double a,double b) {
        double s2 = 0;
        for (int i = 0; i < x.length; i++) {
            s2 += (y[i] - (a + b*x[i]))*(y[i] - (a + b*x[i]));
        }
        return s2 / (x.length - 2);
    }

    private double sb (double s2, double sSquaresX){
        double length = x.length - 1 ;
        return Math.sqrt(s2) / (Math.sqrt(sSquaresX)*Math.sqrt(length));
    }

    private double sa(double s2,double srX,double sSquaresX){
        double length = x.length;
        return Math.sqrt(s2)*Math.sqrt(1/length + (srX*srX)/((length-1)*sSquaresX));
    }

    private double leftb(double criticalValue, double sb){
        return b - criticalValue * sb;
    }

    private double rightb(double criticalValue, double sb){
        return b + criticalValue * sb;
    }

    private double lefta(double criticalValue, double sa){
        return a - criticalValue * sa;
    }

    private double righta(double criticalValue, double sa){
        return a + criticalValue * sa;
    }
}
