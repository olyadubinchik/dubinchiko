package prog.kurs.gui;

import org.jfree.chart.ChartPanel;
import prog.kurs.LineChartHelper;
import prog.kurs.RegressionUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import javax.swing.*;

public class Runner {

    private JButton loadXButton;
    private JButton loadYButton;
    private JList listX;
    private JList listY;
    private JTextArea console;
    private JButton calcRegressionButton;
    private JButton calcYButton;
    private JPanel chartPanel;
    private JPanel mainPanel;

    private RegressionUI regression;

    public Runner() {
        calcYButton.setEnabled(false);

        loadXButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser("./data");
                int returnValue = fileChooser.showOpenDialog(mainPanel);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    try {
                        List<String> lines = Files.readAllLines(Paths.get(selectedFile.toURI()));
                        listX.setListData(lines.toArray(new String[lines.size()]));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        loadYButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser("./data");
                int returnValue = fileChooser.showOpenDialog(mainPanel);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    try {
                        List<String> lines = Files.readAllLines(Paths.get(selectedFile.toURI()));
                        listY.setListData(lines.toArray(new String[lines.size()]));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        calcRegressionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ListModel model = listX.getModel();
                Double[] x = new Double[model.getSize()];
                for (int i = 0; i < model.getSize(); i++) {
                    x[i] = Double.valueOf(String.valueOf(model.getElementAt(i)));
                }
                model = listY.getModel();
                Double[] y = new Double[model.getSize()];
                for (int i = 0; i < model.getSize(); i++) {
                    y[i] = Double.valueOf(String.valueOf(model.getElementAt(i)));
                }
                regression = new RegressionUI(x, y, console);
                ChartPanel lineChartPanel = new LineChartHelper().createLineChartPanel(x, y, regression);
                chartPanel.removeAll();
                chartPanel.setLayout(new BorderLayout());
                chartPanel.add(lineChartPanel);
                chartPanel.revalidate();
                calcYButton.setEnabled(true);
            }
        });

        calcYButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String inputX = JOptionPane.showInputDialog(mainPanel, "Введите значение X");
                // расчет значенич y для inputX
                JOptionPane.showMessageDialog(mainPanel, "Полученное значение Y = " + regression.calculateNew(Double.valueOf(inputX)));
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Runner");
        frame.setContentPane(new Runner().mainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
