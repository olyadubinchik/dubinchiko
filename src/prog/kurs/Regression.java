package prog.kurs;

import umontreal.ssj.probdist.StudentDist;

public class Regression {

    private Double[] x;
    private Double[] y;

    private Double a;
    private Double b;

    public Regression(Double[] x, Double[] y) {
        this.x = x;
        this.y = y;

        calcRegression();
    }

    public String regressionEq() {
        return String.format("y = %sx + %s", b, a);
    }

    public Double calculateNew(Double x) {
        return b*x + a;
    }

    private void calcRegression() {
        Double sumX = sumX();
        System.out.println("Сумма элементов x: " + sumX);

        Double sumY = sumY();
        System.out.println("Сумма элементов y: " + sumY);

        Double sumSquaresX = sumSquaresX();
        System.out.println(sumSquaresX);

        Double sumXY = sumXY();
        System.out.println(sumXY);

        b = b(sumX, sumY, sumXY, sumSquaresX);
        System.out.println("b: " + b);

        a = a(b, sumX, sumY);
        System.out.println("a: " +a);

        Double srX = srX();
        System.out.println(srX);

        Double  sSquaresX =  sSquaresX();
        System.out.println(sSquaresX);

        Double  s2 = s2(a,b);
        System.out.println(s2);

        Double  sb = sb(s2,sSquaresX);
        System.out.println(sb);

        Double  sa = sa( s2, srX, sSquaresX);
        System.out.println(sa);

        double criticalValue = StudentDist.inverseF(x.length - 2, 0.975);
        System.out.println(criticalValue);

        boolean isBSufficient = Math.abs(b) > criticalValue * sb;
        System.out.println(isBSufficient ? "Значение коэффициента b значимо" : "Значение коэффициента b не значимо");

        boolean isASufficient = Math.abs(b) > criticalValue * sa;
        System.out.println(isASufficient ? "Значение коэффициента a значимо" : "Значение коэффициента a не значимо");

        double leftb = leftb(criticalValue, sb);
        double rightb = rightb(criticalValue, sb);
        System.out.println("доверительный интервал для b: [" + leftb + ";" + rightb+ "]" );

        double lefta = lefta(criticalValue, sa);
        double righta = righta(criticalValue, sa);
        System.out.println("доверительный интервал для a: [" + lefta + ";" + righta+ "]" );
    }

    private double sumX() {
        double sumX = 0;
        for (double element : x) {
            sumX += element;
        }
        return sumX;
    }

    private double sumY() {
        double sumY = 0;
        for (double element : y) {
            sumY += element;
        }
        return sumY;
    }

    private double sumSquaresX() {
        double sumSquaresX = 0;
        for (double element : x) {
            sumSquaresX += element * element;
        }
        return sumSquaresX;
    }

    private double sumXY() {
        double sumXY = 0;
        for (int i = 0; i < x.length; i++) {
            sumXY += x[i] * y[i];
        }
        return sumXY;
    }

    private double b(double sumX, double sumY, double sumXY, double sumSquaresX) {
        double length = x.length;
        return (length * sumXY - sumX * sumY) / (length * sumSquaresX - sumX * sumX);
    }

    private double a(double b, double sumX, double sumY) {
        double length = x.length;
        return (sumY - b * sumX) / length;
    }

    private double srX() {
        return sumX()/ x.length;
    }

    private double sSquaresX() {
        double sSquaresX = 0;

        for (int i = 0; i < x.length; i++) {
            sSquaresX += (x[i] - srX())*(x[i] - srX()) ;
        }
        return  sSquaresX / (x.length - 1);
    }

    private double s2 (double a,double b) {
        double s2 = 0;
        for (int i = 0; i < x.length; i++) {
            s2 += (y[i] - (a + b*x[i]))*(y[i] - (a + b*x[i]));
        }
        return s2 / (x.length - 2);
    }

    private double sb (double s2, double sSquaresX){
        double length = x.length - 1 ;
        return Math.sqrt(s2) / (Math.sqrt(sSquaresX)*Math.sqrt(length));
    }

    private double sa(double s2,double srX,double sSquaresX){
        double length = x.length;
        return Math.sqrt(s2)*Math.sqrt(1/length + (srX*srX)/((length-1)*sSquaresX));
    }

    private double leftb(double criticalValue, double sb){
        return b - criticalValue * sb;
    }

    private double rightb(double criticalValue, double sb){
        return b + criticalValue * sb;
    }

    private double lefta(double criticalValue, double sa){
        return a - criticalValue * sa;
    }

    private double righta(double criticalValue, double sa){
        return a + criticalValue * sa;
    }
}
